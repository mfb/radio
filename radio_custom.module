<?php

/**
 * Implements hook_block_info().
 */
function radio_custom_block_info() {
  $blocks['podcast'] = array(
    'cache'  => DRUPAL_CACHE_GLOBAL,
    'info'   => t('Podcast generator'),
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function radio_custom_block_view($delta) {
  switch ($delta) {
    case 'podcast':
      return array(
        'subject' => t('Podcast generator'),
        'content' => drupal_get_form('radio_custom_form_podcast'),
      );
  }
}

/**
 * Load feed items
 *
 * @param $type
 *   The filter for the items. Possible values: 'sum', 'source', 'category'
 * @param $data
 *   Feed or category data for filtering
 * @return
 *   An array of the feed items.
 */
function radio_custom_feed_items_load($type, $data = NULL) {
  $items = array();
  $range_limit = 5;
  switch ($type) {
    case 'sum':
      $result = db_query_range('SELECT i.*, f.title AS ftitle, f.link AS flink FROM {aggregator_item} i INNER JOIN {aggregator_feed} f ON i.fid = f.fid ORDER BY i.timestamp DESC, i.iid DESC', 0, $range_limit);
      break;
    case 'source':
      $result = db_query_range('SELECT * FROM {aggregator_item} WHERE fid = :fid ORDER BY timestamp DESC, iid DESC', 0, $range_limit, array(':fid' => $data->fid));
      break;
    case 'category':
      $result = db_query_range('SELECT i.*, f.title AS ftitle, f.link AS flink FROM {aggregator_category_item} c LEFT JOIN {aggregator_item} i ON c.iid = i.iid LEFT JOIN {aggregator_feed} f ON i.fid = f.fid WHERE cid = :cid ORDER BY timestamp DESC, i.iid DESC', 0, $range_limit, array(':cid' => $data['cid']));
      break;
  }

  foreach ($result as $item) {
    $item->categories = db_query('SELECT c.title, c.cid FROM {aggregator_category_item} ci LEFT JOIN {aggregator_category} c ON ci.cid = c.cid WHERE ci.iid = :iid ORDER BY c.title', array(':iid' => $item->iid))->fetchAll();
    $summary = text_summary($item->description, NULL, variable_get('aggregator_teaser_length', 600));
    if ($summary != $item->description) {
      $summary .= '<p><a href="' . check_url($item->link) . '">' . t('read more') . "</a></p>\n";
    }
    $item->description = $summary;
    $items[] = $item;
  }

  return $items;
}

/**
 * Implements hook_form_alter().
 */
function radio_custom_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'radio_custom_form_podcast') {
    unset($form['form_build_id']);
    unset($form['form_token']);
    unset($form['form_id']);
  }
}

/**
 * Podcast form builder.
 */
function radio_custom_form_podcast() {
  $form['#method'] = 'get';
  $form['#action'] = url('podcast');
  $form['keys'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter your keywords'),
    '#size' => 20,
    '#description' => t('Enter your search terms — for example, <em>cats OR dogs</em> — to generate a keyword-based podcast URL.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
    '#name' => '',
    '#id' => 'radio-block-podcast-submit',
  );
  return $form;
}

/**
 * Implements hook_menu().
 */
function radio_custom_menu() {
  $items = array();
  $type = node_type_get_type('audio');
  $items['publish.html'] = array(
    'title' => 'Publish',
    'page callback' => 'node_add',
    'page arguments' => array('audio'),
    'access callback' => 'node_access',
    'access arguments' => array('create', 'audio'),
    'description' => $type->description,
    'file' => 'node.pages.inc',
    'file path' => drupal_get_path('module', 'node'),
  );
  $items['index.html'] = array(
    'title' => 'Home',
    'page callback' => 'radio_custom_page_category',
    'access arguments' => array('access news feeds'),
    'file' => 'aggregator.pages.inc',
    'file path' => drupal_get_path('module', 'aggregator'),
  );
  $items['about.html'] = array(
    'title' => 'About',
    'page callback' => 'radio_custom_page_about',
    'access callback' => TRUE,
  );
  $items['cities.html'] = array(
    'title' => 'Indymedia',
    'page callback' => 'radio_custom_page_cities',
    'access callback' => TRUE,
  );
  $items['podcast.html'] = array(
    'title' => 'Podcast',
    'page callback' => 'radio_custom_page_podcast',
    'access callback' => TRUE,
  );
  $items['yp/icecast.html'] = array(
    'title' => 'Add your stream',
    'page callback' => 'radio_custom_page_icecast',
    'access callback' => TRUE,
    'type' => MENU_LOCAL_TASK,
  );
  $items['metagen.php'] = array(
    'page callback' => 'radio_custom_page_metagen',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_node_validate().
 */
function radio_custom_node_validate($node, $form, &$form_state) {
  global $is_https;
  if (!$is_https) {
    radio_custom_set_error();
    return;
  }
//  if (geoip_country_code() == 'TR') {
//    radio_custom_set_error();
//    return;
//  }
  $full_text = serialize($node);
  if (strpos($full_text, ' bolum ') !== FALSE || strpos($full_text, ' izle ') !== FALSE) {
    radio_custom_set_error();
    return;
  }
  $pattern = '/[\x{00E7}\x{011F}\x{0131}\x{015F}\x{00F6}\x{00FC}\x{00C7}\x{011E}\x{0130}\x{015E}\x{00D6}\x{00DC}]/u';
  if (preg_match($pattern, $full_text)) {
    radio_custom_set_error();
    return;
  }
  foreach ($node->upload as $language) {
    foreach ($language as $delta) {
      if ($delta['fid']) {
        $file = file_load($delta['fid']);
        // Do not allow file to be re-used in another node.
        $usage = file_usage_list($file);
        if (!empty($usage['file']['node'])) {
          foreach ($usage['file']['node'] as $nid => $count) {
            if ($nid != $node->nid) {
              radio_custom_set_error();
              return;
            }
          }
        }
        if ($file->filesize < 2000000) {
          radio_custom_set_error();
          return;
        }
      }
    }
  }
  foreach ($node->body as $language) {
    foreach ($language as $delta) {
      if ($count = substr_count(strtolower($delta['value']), 'http:')) {
        radio_custom_set_error();
        return;
      }
    }
  }
}

/**
 * About page callback.
 */
function radio_custom_page_about() {
  return node_page_view(node_load(2453));
}

/**
 * Menu callback; displays all the items aggregated in a particular category.
 *
 * @param $category
 *   The category for which to list all the aggregated items.
 *
 * @return
*   The rendered list of items for a category.
 */
function radio_custom_page_category($cid = 1) {
  // Turn off the page title for front page.
  drupal_set_title('');
  $category = aggregator_category_load($cid);
  drupal_add_feed('rss.xml', variable_get('site_name', 'Drupal') . ' ' . t('RSS'));
  drupal_add_feed('aggregator/rss/' . $category['cid'], variable_get('site_name', 'Drupal') . ' ' . t('aggregator - @title', array('@title' => $category['title'])));

  // It is safe to include the cid in the query because it's loaded from the
  // database by aggregator_category_load.
  $items = radio_custom_feed_items_load('category', $category);

  return _aggregator_page_list($items, arg(3)) . '<div id="radio-more-link">' . l(t('More'), 'aggregator/categories/1'). '</div>';
}

/**
 * Cities page callback.
 */
function radio_custom_page_cities() {
  return node_page_view(node_load(18501));
}

/**
 * Icecast page callback.
 */
function radio_custom_page_icecast() {
  return node_page_view(node_load(17168));
}

/**
 * Metagen page callback.
 */
function radio_custom_page_metagen() {
  $url = isset($_GET['url']) ? $_GET['url'] : NULL;
  $output = $url . "\r\n";
  drupal_add_http_header('Content-Type', 'audio/x-mpegurl');
  drupal_add_http_header('Content-Length', strlen($output));
  drupal_add_http_header('Content-Disposition', 'inline; filename='. preg_replace(array(';http://;', ';[^a-z0-9];'), array('', '-'), $url) . '.m3u');
  print $output;
}

/**
 * Podcast page callback.
 */
function radio_custom_page_podcast() {
  return node_page_view(node_load(18514));
}

/**
 * Node validation helper function.
 */
function radio_custom_set_error() {
  sleep(30);
  form_set_error('', t('Sorry, please try again in 30 seconds. For help please contact imc-audio@lists.indymedia.org.'));
}
