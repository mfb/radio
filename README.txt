RADIO.INDYMEDIA.ORG
-------------------

Chances are you only want to install the Radio Features module.

This module requires several contributed modules:
- http://drupal.org/project/ctools
- http://drupal.org/project/features
- http://drupal.org/project/rss_field_formatters
- http://drupal.org/project/transliteration
- http://drupal.org/project/views

Contact: imc-audio@lists.indymedia.org
