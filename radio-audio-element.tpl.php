<audio style="width: 100%" itemscope itemtype="http://schema.org/AudioObject" controls="controls" preload="none" title="<?php echo $title; ?>">
  <source src="<?php echo $src; ?>" type="<?php echo $type; ?>" />
  <meta itemprop="contentURL" content="<?php echo $src; ?>" />
  <meta itemprop="contentSize" content="<?php echo $file->filesize; ?>" />
  <meta itemprop="description" content="<?php echo $title; ?>" />
  <meta itemprop="encodingFormat" content="<?php echo $extension; ?>" />
  <meta itemprop="name" content="<?php echo $filename; ?>" />
</audio>
