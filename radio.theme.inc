<?php

/**
 * Process variables for radio-audio-element.tpl.php
 *
 * @param $variables
 *   An associative array containing:
 *   - file: A file object for which the audio element will be created.
 */
function template_preprocess_radio_audio_element(&$variables) {
  $variables['extension'] = check_plain(pathinfo($variables['file']->uri, PATHINFO_EXTENSION));
  $variables['filename'] = check_plain($variables['file']->filename);
  $variables['src'] = check_url(file_create_url($variables['file']->uri));
  $variables['title'] = check_plain(empty($variables['file']->description) ? $variables['file']->filename : $variables['file']->description);
  $variables['type'] = check_plain($variables['file']->filemime);
}

/**
 * Returns HTML for a file attachments table.
 *
 * @param $variables
 *   An associative array containing:
 *   - items: An array of file attachments.
 *
 * @ingroup themeable
 */
function theme_radio_audio_table($variables) {
  $output = '';
  foreach ($variables['items'] as $delta => $item) {
    $output .= theme('table', ['rows' => [
      [theme('radio_audio_element', ['file' => (object) $item])],
      [theme('file_link', ['file' => (object) $item])],
      [format_size($item['filesize'])],
    ]]);
  }
  return $output;
}
